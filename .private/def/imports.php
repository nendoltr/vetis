<?php
//Keywords for a lightweight code !

//constants for database
define('DB_NAME', 'veteri');
define('DB_HOST', 'localhost');
define('DB_PASS', '');
define('DB_USER', 'root');


//urls
define('ROOT', '../');

//for public
define('FONCT', '.public/model/');
define('PAGES', '.public/pages/');
define('CSS', '.public/assets/css');
define('JS', '.public/assets/js');
define('IMG', '.public/assets/img');
define('INCLUDES', '.public/includes/');

//for private
define('_MODEL', '.private/model/');
define('_DATA', '.private/data/');
define('_DEF', '.private/def/');

//css and JS files
